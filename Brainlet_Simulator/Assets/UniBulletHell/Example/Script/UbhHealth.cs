﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UbhHealth : MonoBehaviour
{
    public UbhGameManager manager;
    public int health;
    public int numOfBrains;
    public Image[] brains;
    public Sprite fullBrain;
    public Sprite emptyBrain;
    private bool invincible = false;
    private float invStartTime;
    private float invTimeRemaining;


    public int GetHealth()
    {
        return health;
    }

    public void SetHealth(int val)
    {
        health = val;
        numOfBrains = val;
    }

    public bool GetInvincible()
    {
        return invincible;
    }

    public void SetInvincible(float invTime)
    {
        invincible = true;
        invStartTime = Time.time;
        invTimeRemaining = invTime;
        manager.GetPlayer().GetComponent<Animator>().Play("player@invincible");
    }

    public void Damage(int damageAmount)
    {
        if (!invincible && manager.getNumLosses() < 3)
        {
            health -= damageAmount;
            Debug.Log("Took damage, health is: " + health);
            if (health < 0) health = 0;
        }
    }

    private void Update()
    {
        if (invincible)
        {
            invTimeRemaining = invTimeRemaining - (Time.time - invStartTime);
            if (invTimeRemaining <= 0)
            {
                invincible = false;
            }
        }

        if (health > numOfBrains) {
            health = numOfBrains;
        }

        for (int i = 0; i < brains.Length; i++) {
            if (i < health) {
                brains[i].sprite = fullBrain;
            } else {
                brains[i].sprite = emptyBrain;
            }

            if (i < numOfBrains) {
                brains[i].enabled = true;
            } else {
                brains[i].enabled = false;
            }
        }
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Serialization;
using UnityEngine.UI;


public class UbhGameManager : UbhMonoBehaviour
{
    public const int BASE_SCREEN_WIDTH = 600;
    public const int BASE_SCREEN_HEIGHT = 450;

    [FormerlySerializedAs("_ScaleToFit")]
    public bool m_scaleToFit = false;

    [SerializeField, FormerlySerializedAs("_PlayerPrefab")]
    private GameObject m_playerPrefab = null;
    [SerializeField, FormerlySerializedAs("_GoTitle")]
    private GameObject m_goTitle = null;
    [SerializeField, FormerlySerializedAs("_GoLetterBox")]
    private GameObject m_goLetterBox = null;
    [SerializeField, FormerlySerializedAs("_Score")]
    private UbhScore m_score = null;
    public UbhHealth m_health = null;
    public UbhLevelGenerator levelGenerator = null;
    public BulletTerminalManager bulletTermManager = null;
    static private int numLosses = 0;
    public GameObject m_player = null;
    public Text endText;
    private static bool hasRestarted = false;

    public GameObject BulletScene;



    private void Start()
    {
        if (!hasRestarted)
        {
            hasRestarted = true;
            Destroy(BulletScene);
            SceneManager.UnloadSceneAsync("UniBulletHell/Example/UBH_GameExample");
            SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
        }
        Debug.Log("Num Losses: " + numLosses);
        m_goLetterBox.SetActive(m_scaleToFit == false);
        AudioManagers.main.StopSFX("frosty_snowman");
        AudioManagers.main.StopSFX("full_trombone_song");
        GameStart();
        HideText(); // default center text hidden at start. Use ShowText
    }

    private void Update()
    {
    }

    private void OnEnable()
    {
        BulletTerminalManager.onQuestionFinished += OnAnswer;
    }

    private void OnDisable()
    {
        BulletTerminalManager.onQuestionFinished -= OnAnswer;
    }

    public void OnLevelWin()
    {
        StartCoroutine(_OnLevelWin());
    }

    private IEnumerator _OnLevelWin()
    {
        // TODO: could do different things based on the level

        int level = UbhLevelGenerator.levelSelected;
        m_health.SetInvincible(20.0f);
        levelGenerator.EndLevel();
        ShowText("Neurons activated.  You got smarter!");
        hasRestarted = false; // so it restarts itself next time bullet hell loads for blue bug
        AudioManagers.main.PlaySFX("neurons_activated_2");
        yield return new WaitForSeconds(2);
        if(Anwser.canMusic)
        {
            AudioManagers.main.PlaySFX("full_trombone_song");
        }

        GameManager.DougLevelActive = true;
        if(GameManager.lastClickedWasAnswer)
        {
            Anwser.currentAnswer.textBox.SetActive(true);
        } else
        {
            InteractableDoorTaxiNASAGuy.current.textBox.SetActive(true);
        }
        GameManager.instance.StartCoroutine(GameManager.instance._killTextBoxInSeconds(3));

        HideText();

        Destroy(BulletScene);
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        SceneManager.UnloadSceneAsync("UniBulletHell/Example/UBH_GameExample");

        // TODO: Finished?  Go back to other scene?
    }



    public int getNumLosses()
    {
        return numLosses;

    }

    private void ShowText(string text)
    {
        endText.gameObject.SetActive(true);
        endText.text = text;
    }

    private void HideText()
    {
        endText.gameObject.SetActive(false);
    }

    private void OnAnswer(BulletTerminalManager.AnswerType answer)
    {
        int choice;
        switch(answer)
        {
            case BulletTerminalManager.AnswerType.ANSWERED_CORRECTLY:
                Debug.Log("ANSWERED CORRECTLY!!");
                AudioManagers.main.PlaySFX("correct_answer");
                break;

            case BulletTerminalManager.AnswerType.ANSWERED_INCORRECTLY:
                Debug.Log("ANSWERED WRONGLY!!");

                // play killed audio
                choice = Random.Range(0, 3);
                if (choice == 0)
                {
                    AudioManagers.main.PlaySFX("buddy_incorrect");
                }
                else if (choice == 1)
                {
                    AudioManagers.main.PlaySFX("nexttime");
                }
                else if (choice == 2)
                {
                    AudioManagers.main.PlaySFX("incorrect_answer");
                }

                Damage(1);
                break;

            case BulletTerminalManager.AnswerType.RAN_OUT_OF_TIME:
                Debug.Log("RAN OUT OF TIME!!");
                AudioManagers.main.PlaySFX("oh_no");
                Damage(1);
                break;

            default:
                Debug.LogError("BROOOOOOO");
                break;
        }
    }

    private void GameStart()
    {
        if (m_score != null)
        {
            m_score.Initialize();
        }
        if (m_goTitle != null)
        {
            StartCoroutine(ToggleStartScreen(3.0f));
        }
    }

    public void GameOver()
    {
        numLosses++;
        Destroy(BulletScene);
        SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
        
    }

    public int GetHealth()
    {
        return m_health.GetHealth();
    }

    public void Damage(int damageAmount)
    {
        m_health.Damage(damageAmount);
        m_health.SetInvincible(3.0f);
    }

    private void CreatePlayer()
    {
        m_player = Instantiate(m_playerPrefab, m_playerPrefab.transform.position, m_playerPrefab.transform.rotation);
    }

    public GameObject GetPlayer()
    {
        return m_player;
    }

    public bool IsPlaying()
    {
        if (m_goTitle != null)
        {
            return m_goTitle.activeSelf == false;
        }
        else
        {
            // for UBH_ShotShowcase scene.
            return true;
        }
    }

    IEnumerator ToggleStartScreen(float seconds)
    {
        m_goTitle.SetActive(true);
        yield return new WaitForSeconds(seconds);

        m_goTitle.SetActive(false);

        int initialHealth = 4;
        //if(UbhLevelGenerator.levelSelected == 5)
        //{
        //    initialHealth += 3;
        //}
        m_health.SetHealth(initialHealth + numLosses);
        levelGenerator.StartLevel();
        bulletTermManager.StartLevel(UbhLevelGenerator.levelSelected);
        CreatePlayer();
        OnLevelWin();
    }
}

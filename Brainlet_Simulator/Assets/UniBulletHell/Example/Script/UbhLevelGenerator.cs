﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UbhLevelGenerator : UbhMonoBehaviour
{
    // Start is called before the first frame update
    public UbhLevel[] levels;
    public static int levelSelected;

    public void Awake()
    {
        // set levelSelected to current level
    }

    public void StartLevel()
    {
        StartCoroutine(levels[levelSelected].BeginLevel());
    }

    public void EndLevel()
    {
        levels[levelSelected].EndLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

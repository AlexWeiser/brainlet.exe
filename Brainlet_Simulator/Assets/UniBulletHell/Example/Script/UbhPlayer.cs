﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(UbhSpaceship))]
public class UbhPlayer : UbhMonoBehaviour
{
    public const string NAME_ENEMY_BULLET = "EnemyBullet";
    public const string NAME_ENEMY = "Enemy";

    private const string AXIS_HORIZONTAL = "Horizontal";
    private const string AXIS_VERTICAL = "Vertical";

    private readonly Vector2 VIEW_PORT_LEFT_BOTTOM = UbhUtil.VECTOR2_ZERO;
    private readonly Vector2 VIEW_PORT_RIGHT_TOP = UbhUtil.VECTOR2_ONE;

    [SerializeField, FormerlySerializedAs("_UseAxis")]
    private UbhUtil.AXIS m_useAxis = UbhUtil.AXIS.X_AND_Y;

    private UbhSpaceship m_spaceship;
    private UbhGameManager m_manager;
    private Transform m_backgroundTransform;
    private Vector2 m_tempVector2 = UbhUtil.VECTOR2_ZERO;

    private void Start()
    {
        m_spaceship = GetComponent<UbhSpaceship>();
        m_manager = FindObjectOfType<UbhGameManager>();
        m_backgroundTransform = FindObjectOfType<UbhBackground>().transform;
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(VIEW_PORT_LEFT_BOTTOM);
        Vector2 max = Camera.main.ViewportToWorldPoint(VIEW_PORT_RIGHT_TOP); 
        Vector2 pos = transform.position;

        Vector3 vec = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.x = Mathf.Clamp(vec.x, min.x, max.x);
        pos.y = Mathf.Clamp(vec.y, min.y, max.y);
        transform.SetPosition(pos.x, pos.y, transform.position.z);
    }

    private void Damage()
    {
        if (m_manager != null)
        {
            AudioManagers.main.PlaySFX("oh_no");
            m_manager.Damage(1);
            if (m_manager.GetHealth() <= 0)
            {
                m_manager.GameOver();
                m_spaceship.Explosion();
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        HitCheck(c.transform);
    }

    private void OnTriggerEnter(Collider c)
    {
        HitCheck(c.transform);
    }

    private void HitCheck(Transform colTrans)
    {
        // *It is compared with name in order to separate as Asset from project settings.
        //  However, it is recommended to use Layer or Tag.
        string goName = colTrans.name;
        if (goName.Contains(NAME_ENEMY_BULLET))
        {
            UbhBullet bullet = colTrans.GetComponentInParent<UbhBullet>();
            if (bullet.isActive)
            {
                UbhObjectPool.instance.ReleaseBullet(bullet);
                Damage();
            }
        }
        else if (goName.Contains(NAME_ENEMY))
        {
            Damage();
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

public class UbhLevel : UbhMonoBehaviour
{
    [SerializeField, FormerlySerializedAs("_Waves")]
    private GameObject[] m_waves = null;

    private int m_currentWave;
    private UbhGameManager m_manager;
    private GameObject currWave;
    private bool levelActive;

    public void Start()
    {

    }

    public IEnumerator BeginLevel()
    {
        levelActive = true;
        if (m_waves.Length == 0)
        {
            yield break;
        }

        m_manager = FindObjectOfType<UbhGameManager>();

        while (levelActive)
        {

            while (m_manager.IsPlaying() == false)
            {
                yield return null;
            }

            currWave = (GameObject)Instantiate(m_waves[m_currentWave], transform);
            Transform waveTrans = currWave.transform;
            waveTrans.position = transform.position;

            while (0 < waveTrans.childCount)
            {
                yield return null;
            }

            Destroy(currWave);

            m_currentWave = (int)Mathf.Repeat(m_currentWave + 1f, m_waves.Length);
        }
    }

    public void EndLevel()
    {
        levelActive = false;
        Destroy(currWave);
    }
}
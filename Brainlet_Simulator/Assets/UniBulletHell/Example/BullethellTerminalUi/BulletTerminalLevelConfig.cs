﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BulletTerminalLevelConfig
{
    public class Question
    {
        public string ask;
        public string answer;
        public float startTime;
        public float timeToAnswer;
    };

    public static List<List<Question>> levels;

    // static class initializer, instantiate the question
    static BulletTerminalLevelConfig()
    {
        levels = new List<List<Question>>();

        // LEVEL 0
        levels.Add(new List<Question>() {
            new Question
            {
                ask = "At the end of your arms are your...",
                answer = "hands",
                startTime = 3.0f,
                timeToAnswer = 8.0f,
            },
            new Question
            {
                ask = "At the end of your legs are your...",
                answer = "feet",
                startTime = 15.0f,
                timeToAnswer = 8.0f,
            },
            new Question
            {
                ask = "On your butt you have two...",
                answer = "cheeks",
                startTime = 30.0f,
                timeToAnswer = 10.0f,
            }
        });

        // LEVEL 1
        levels.Add(new List<Question>() {
            new Question
            {
                ask = "What is the biggest number in this list: \"1, 2, 3\"",
                answer = "3",
                startTime = 5.0f,
                timeToAnswer = 5.0f,
            },
            new Question
            {
                ask = "If I have 3 apples and I eat 1 apple, how many apples do I have?",
                answer = "2",
                startTime = 15.0f,
                timeToAnswer = 5.0f,
            },
            new Question
            {
                ask = "If you have 15 pennies, how many cents do you have?",
                answer = "15",
                startTime = 25.0f,
                timeToAnswer = 5.0f,
            },
        });

        // LEVEL 2 (alphabet)
        levels.Add(new List<Question>() {
            new Question
            {
                ask = "How do you spell cat?",
                answer = "cat",
                startTime = 5.0f,
                timeToAnswer = 5.0f,
            },
            new Question
            {
                ask = "How do you spell incredulous?",
                answer = "incredulous",
                startTime = 15.0f,
                timeToAnswer = 8.0f,
            },
            new Question
            {
                ask = "Type out the ENTIRE alphabet!",
                answer = "abcdefghijklmnopqrstuvwxyz",
                startTime = 25.0f,
                timeToAnswer = 25.0f,
            },
        });

        // LEVEL 3 (learn manners)
        levels.Add(new List<Question>() {
            new Question
            {
                ask = "How to greet someone? (type a letter) \n" + 
                "a: Please Die \n" + 
                "b: Hello \n" +
                "c: I'm the prip ripper, here to rip your prips",
                answer = "b",
                startTime = 5.0f,
                timeToAnswer = 5.0f,
            },
            new Question
            {
                ask = "How to say goodbye? (type a letter) \n" +
                "a: Seishikina sayōnara \n" +
                "b: Hello \n" + 
                "c: Goodbye",
                answer = "c",
                startTime = 15.0f,
                timeToAnswer = 5.0f,
            },
            new Question
            {
                ask = "If Johnny kicked you in the shin and spit in your face, what should you do? (type a letter) \n" +
                "a: Poke out his eyes \n" +
                "b: Shatter his knees \n" +
                "c: Love and respect him",
                answer = "c",
                startTime = 25.0f,
                timeToAnswer = 5.0f,
            },
        });

        // LEVEL 4
        levels.Add(new List<Question>() {
            new Question
            {
                ask = "Analyze a Carnot cycle for an ideal gas, and show explicitly using the equation of state of the ideal gas and the First Law of Thermodynamics that: Q1/Q2 = T1/T2",
                answer = "yes",
                startTime = 5.0f,
                timeToAnswer = 10.0f,
            },
            new Question
            {
                ask = "Prove that adiabats (lines of constant entropy) have a steeper slope than isotherms (lines of constant temperature) for an ideal gas on a p - v diagram, where the pressure p is the ordinate and the volume per unit mass v the abscissa.",
                answer = "yes",
                startTime = 20.0f,
                timeToAnswer = 10.0f,
            },
            new Question
            {
                ask = "Prove that it is impossible for two lines corresponding to reversible adiabatic processes on a thermodynamic diagram to intersect. (HINT: Assume that they do intersect and complete a cycle with with an isothermal line.Show that the performance of this cycle would violate the second law.)",
                answer = "yes",
                startTime = 35.0f,
                timeToAnswer = 10.0f,
            },
        });

        // LEVEL 5
        levels.Add(new List<Question>() {
            new Question
            {
                ask = "What does this code return?\n" +
                "x = 5; \n" +
                "if (x == 5) {\n" +
                "  print \"big brain\"\n" +
                "}",
                answer = "big brain",
                startTime = 5.0f,
                timeToAnswer = 8.0f,
            },
            new Question
            {
                ask = "Is PHP really that bad?",
                answer = "yes",
                startTime = 15.0f,
                timeToAnswer = 5.0f,
            },
            new Question
            {
                ask = "Whoever coded this is:\n" +
                "a: A galaxy brain\n" +
                "b: Not a galaxy brain",
                answer = "a",
                startTime = 25.0f,
                timeToAnswer = 5.0f,
            },
        });
    }

    // questions
    // answers
    // when each question comes in
    // how much time user has for that question

    // callback for when question finished
    // answered correctly, answered incorrectly, time ran out (ENUM?)
}

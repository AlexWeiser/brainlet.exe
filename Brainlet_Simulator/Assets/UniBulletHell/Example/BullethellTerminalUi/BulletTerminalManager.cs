﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BulletTerminalManager : MonoBehaviour
{
    public TMP_InputField inputField;
    public TMP_Text questionText;
    public Image timeMeterImage;
    public GameObject bottomLeftUI;

    [Tooltip("Start automically if true.  Otherwise, must call StartLevel(levelNum) on object to begin")]
    public bool startImmediately = false;
    public int levelNumber = 0;

    private QuestionRunner questionRunner;

    public enum AnswerType
    {
        ANSWERED_CORRECTLY,
        ANSWERED_INCORRECTLY,
        RAN_OUT_OF_TIME,
    };

    public delegate void OnQuestionFinished(AnswerType answerType);
    public static event OnQuestionFinished onQuestionFinished;

    void Start()
    {
        bottomLeftUI.SetActive(false);

        if (startImmediately)
        {
            StartLevel(levelNumber);
        }
    }

    public void StartLevel(int _level)
    {

        questionRunner = new QuestionRunner();
        foreach (BulletTerminalLevelConfig.Question question in BulletTerminalLevelConfig.levels[_level])
        {
            StartCoroutine(LaunchQuestionWhenTime(question));
        }

        switch (_level)
        {
            case 0:
            case 1:
            case 2:
                AudioManagers.main.PlaySFX("bullet_hell_answer_the_questions");
                break;

            case 3:
            case 4:
            case 5:
                AudioManagers.main.PlaySFX("electrify_neurons");
                break;
        }
    }

                
    private void Update()
    {
        inputField.ActivateInputField();
        if (questionRunner.isRunning)
        {
            questionRunner.Update();
            UpdateTimeBar();
            if(questionRunner.timeLeft <= 0)
            {
                Debug.Log("Question FINISHED.");
                questionRunner.isRunning = false;
                bottomLeftUI.SetActive(false);
                onQuestionFinished(AnswerType.RAN_OUT_OF_TIME);
            }


            if (Input.GetKeyDown("return"))
            {
                bottomLeftUI.SetActive(false);

                // Correct Answer (case insensitive)
                if (System.String.Equals(inputField.text, questionRunner.question.answer, System.StringComparison.OrdinalIgnoreCase))
                {
                    Debug.Log("Correct Answer!");
                    onQuestionFinished(AnswerType.ANSWERED_CORRECTLY);
                }
                else
                {
                    Debug.Log("INCORRECT ANSWER");
                    onQuestionFinished(AnswerType.ANSWERED_INCORRECTLY);
                }

                questionRunner.isRunning = false;
            }   
        }
    }

    private void UpdateTimeBar()
    {
        timeMeterImage.fillAmount = questionRunner.fractionTimeLeft;
    }

    private IEnumerator LaunchQuestionWhenTime(BulletTerminalLevelConfig.Question question)
    {
        yield return new WaitForSeconds(question.startTime);
        inputField.text = "";
        questionText.text = question.ask;
        bottomLeftUI.SetActive(true);
        questionRunner.RunQuestion(question);
    }

    private class QuestionRunner
    {
        public float timeLeft;
        public float launchTime;
        public float fractionTimeLeft;
        public bool isRunning = false;
        public BulletTerminalLevelConfig.Question question;

        public void RunQuestion(BulletTerminalLevelConfig.Question _question)
        {
            Debug.Log("Running Question: " + _question.ask);
            question = _question;
            timeLeft = question.timeToAnswer;
            launchTime = Time.time;
            isRunning = true;
            
            if(question.answer == "abcdefghijklmnopqrstuvwxyz")
            {
                AudioManagers.main.PlaySFX("entire_abc");
            }
        }

        public void Update()
        {
            timeLeft = question.timeToAnswer - (Time.time - launchTime);
            fractionTimeLeft = timeLeft / question.timeToAnswer;
            if(fractionTimeLeft < 0)
            {
                fractionTimeLeft = 0;
            }
        }
    }
}

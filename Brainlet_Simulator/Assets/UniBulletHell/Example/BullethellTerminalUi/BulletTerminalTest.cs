﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTerminalTest : MonoBehaviour
{
    void DoSomething(BulletTerminalManager.AnswerType answer)
    {
        Debug.Log("DOING SOMETHING BULLET TERMINAL CALLBACK");
        Debug.Log(answer);
    }

    private void OnEnable()
    {
        Debug.Log("adding callback for bullet terminal manager");
        BulletTerminalManager.onQuestionFinished += DoSomething;
    }

    private void OnDisable()
    {
        BulletTerminalManager.onQuestionFinished -= DoSomething;
    }






}







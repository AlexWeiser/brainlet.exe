﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrainUIManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Image[] images;
    private void Start()
    {
        SetBrain(0);
    }
    // Update is called once per frame
    void Update()
    {
        SetBrain(Intelligence.IntelligenceNumber);

        // SetBrain(Random.Range(0, images.Length));
    }

    public void SetBrain(int brain)
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].gameObject.SetActive(false);
        }
        images[brain].gameObject.SetActive(true);
    }
}

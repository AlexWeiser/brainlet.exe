﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Anwser : MonoBehaviour
{
    public bool isCorrect;

    public GameObject body;
    public GameObject pants;
    public GameObject shoes;
    public Material blue;
    public Material red;
    public Material green;

    // before taxi: hands, speak english, play music, see colors, make pleasant conversation, math

    public static bool handsWork,canRead,canMath,canMusic,canSmell,canHorniculture,canColor,canSocial,canAstro,canPhysics,canAlien,canCode = false;
    public GameObject textBox;
    public Text textField; 

    public GameObject dougLevel;

    public static Anwser currentAnswer;

    public static bool Fall = false;

    public void SelectedAnswer()
    {
        currentAnswer = this;
        GameManager.lastClickedWasAnswer = true;

        GameObject TextBox = GameObject.FindGameObjectWithTag("Question");
        TextBox.SetActive(false);
        MouseInteract.Interacting = false;

        //WRONG
        if (isCorrect == true)
        {
            Intelligence.IntelligenceNumber++;
            Debug.Log("" + Intelligence.IntelligenceNumber);

            Invoke("ResetText", 2f);
            if(GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Color"))
            {
                textBox.SetActive(true);
                body.GetComponent<Renderer>().material = blue;
                pants.GetComponent<Renderer>().material = red;
                shoes.GetComponent<Renderer>().material = green;
                canColor = true;
                textField.text = "You learned about colors!";
                 
            }
            if(GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Hands"))
            {
                handsWork = true;
                textField.text = "You learned motor control!";
                UbhLevelGenerator.levelSelected = 0;
                SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
                GameManager.DougLevelActive = false;

                GameManager.DougLevel = GameObject.FindGameObjectWithTag("CurrentLevel");
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Book"))
            {
                canRead = true;
                textField.text = "You learned the alphabet!";
                UbhLevelGenerator.levelSelected = 2;
                SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
                GameManager.DougLevelActive = false;
                GameManager.DougLevel = GameObject.FindGameObjectWithTag("CurrentLevel");
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Math"))
            {
                canMath = true;
                textField.text = "You learned how to do math!";
                UbhLevelGenerator.levelSelected = 1;
                SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
                GameManager.DougLevelActive = false;
                GameManager.DougLevel = GameObject.FindGameObjectWithTag("CurrentLevel");
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Music"))
            {
                textBox.SetActive(true);
                canMusic = true;
                textField.text = "You learned about music!";
                AudioManagers.main.StopSFX("frosty_snowman");
                AudioManagers.main.PlaySFX("full_trombone_song");
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Tree"))
            {
                textBox.SetActive(true);
                canHorniculture = true;
                textField.text = "You learned about horticulture!";
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Poop"))
            {
                textBox.SetActive(true);
                canSmell = true;
                textField.text = "You learned how to smell!";
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Social"))
            {
                canSocial = true;
                UbhLevelGenerator.levelSelected = 3;
                SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
                GameManager.DougLevelActive = false;
                GameManager.DougLevel = GameObject.FindGameObjectWithTag("CurrentLevel");

                //textField.text = "";
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Alien"))
            {
                textBox.SetActive(true);
                canAlien = true;
                textField.text = "You learned how to speak Zirgleplonie!";
                
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Code"))
            {
                canCode = true;
                textField.text = "Wow you're getting really smart!";
                UbhLevelGenerator.levelSelected = 5;
                SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
                GameManager.DougLevelActive = false;
                GameManager.DougLevel = GameObject.FindGameObjectWithTag("CurrentLevel");
            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Phyiscs"))
            {
                canPhysics = true;
                textField.text = "You learned Thermonuclear Astrophysics!";
                UbhLevelGenerator.levelSelected = 4;
                SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
                GameManager.DougLevelActive = false;
                GameManager.DougLevel = GameObject.FindGameObjectWithTag("CurrentLevel");


            }
            if (GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Astro"))
            {
                textBox.SetActive(true);
                canAstro = true;
                textField.text = "You learned about the Universe!";
            }

            Destroy(GameManager.currentInteraction);
        }
        // question was WRONG
        else
        {
            Fall = true;
            AudioManagers.main.PlaySFX("grunt7");
            Invoke("StopFalling", 2.5f);
        }
        
    }
   void ResetText()
    {
        textBox.SetActive(false);
    }
    void StopFalling()
    {
        Fall = false;
        MouseInteract.Interacting = false;
    }
}

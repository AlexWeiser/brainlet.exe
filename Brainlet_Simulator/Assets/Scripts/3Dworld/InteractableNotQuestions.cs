﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractableNotQuestions : MonoBehaviour
{
    public GameObject textBox;
    public Text textField;

    public string Op1;
    public string Op2;
    public string Op3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnMouseOver()
    {
        if (MouseInteract.Interacting == false) 
        {
            GameManager.currentInteraction = this.gameObject;
         }
    }
    void OnMouseUp()
    {
        if (GameManager.dist < 2.5f)
        {
            textBox.SetActive(true);
            switch (Intelligence.IntelligenceNumber)
            {
                case 0:
                case 1:
                    textField.text = Op1;
                    break;
                case 2:
                    textField.text = Op2;
                    break;
                case 3:
                    textField.text = Op3;
                    break;


            }
            if (gameObject.tag == "Taxi" && Anwser.canRead == true)
            {
                //Destroy(gameObject);
                //Voice Clip
                textField.text = "Lets go to NASA!";
                SceneManager.LoadScene("3DNASA", LoadSceneMode.Single);
            }
            else if (gameObject.tag == "Door" && Anwser.handsWork == true)
            {
                //Destroy(gameObject);
                textField.text = "You opened the Door!";
                SceneManager.LoadScene("3DOutside", LoadSceneMode.Single);
            }
            
            Invoke("Reset", 1f);
        }
    }
    void Reset()
    {
        textBox.SetActive(false);
    }
}

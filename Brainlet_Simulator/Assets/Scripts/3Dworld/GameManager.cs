﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameObject currentInteraction;
    public GameObject DontDestroy;
    public GameObject player;
    public static GameObject DougLevel;
  
    public static float dist = 2.5f;
    public static bool DougLevelActive = true;
    public static bool lastClickedWasAnswer = false;

    public static GameManager instance;

    void Awake()
    { //called when an instance awakes in the game
        instance = this; //set our static reference to our newly initialized instance
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(DontDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        if ( DougLevelActive == false)
        {
            
            DougLevel.SetActive(false);
            DontDestroy.SetActive(false);

        }
        if( DougLevelActive == true)
        {
            /// TODO this is throwing tons of errors, what is DougLevel for?
            if(DougLevel != null)
            {
                DougLevel.SetActive(true);
            }
            DontDestroy.SetActive(true);
        }
        if (currentInteraction != null)
        {
            dist = Vector3.Distance(currentInteraction.transform.position, player.transform.position);
           // Debug.Log(dist + "");
        }
    }

    // kills whatever the current showing text box is
    public IEnumerator _killTextBoxInSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        if (GameManager.lastClickedWasAnswer)
        {
            Anwser.currentAnswer.textBox.SetActive(false);
        }
        else
        {
            InteractableDoorTaxiNASAGuy.current.textBox.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CicktoMove : MonoBehaviour {

    Animator dougAnim;
    NavMeshAgent dougNav;

    bool Running;


	// Use this for initialization
	void Start () {
        dougAnim = GetComponent<Animator>();
        dougNav = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Input.GetMouseButtonDown(0) && MouseInteract.Interacting == false)
        {
            if(!Anwser.canRead)
            {
                float frac = Random.Range(0f, 1f);
                if(frac < 0.35f)
                {
                    AudioManagers.main.PlaySFX("grunt" + Random.Range(1, 7));
                }
            }
            if (Physics.Raycast(ray, out hit, 100))
            {
                dougNav.destination = hit.point;
            }
        }
        if (dougNav.remainingDistance <= dougNav.stoppingDistance)
        {
            Running = false;
            dougAnim.SetFloat("speed", 0);
        }
        else
        {
           Running = true;
           dougAnim.SetFloat("speed", 1);
        }
        dougAnim.SetFloat("canWalk", Intelligence.IntelligenceNumber );
        
        if(Anwser.Fall == true)
        {
            dougAnim.SetBool("Fall", true);
        }
        else
        {
            dougAnim.SetBool("Fall", false);
        }
    }
}

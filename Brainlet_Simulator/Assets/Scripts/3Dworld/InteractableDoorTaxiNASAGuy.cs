﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractableDoorTaxiNASAGuy : MonoBehaviour
{
    public GameObject Exclaim;
    public GameObject textBox;
    public Text textField;

    public static bool Interacting = false;
    public static bool InteractionInProgress = false;
    public static bool finalSceneLoaded = false;

    public static InteractableDoorTaxiNASAGuy current;

    void OnMouseOver()
    {
        Exclaim.SetActive(true);
        if (Interacting == false)
        {
            GameManager.currentInteraction = this.gameObject;
        }
    }

    private void Start()
    {
        InteractionInProgress = false;
    }

    void OnMouseExit()
    {
        Exclaim.SetActive(false);
    }

    void OnMouseUp()
    {
        Interacting = true;
        GameManager.lastClickedWasAnswer = false;
        current = this;
    }

    // Character has reached object, show canvas so user can answer question
    public void ActivateAction()
    {

        if (gameObject.tag == "Door")
        {
            DoorAction();
        }

        if (gameObject.tag == "Taxi")
        {
            StartCoroutine(TaxiAction());
        }

        if (gameObject.tag == "NasaGuy")
        {
            StartCoroutine(NasaGuyAction());
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && Interacting && !InteractionInProgress && !textBox.activeSelf)
        {
            ActivateAction();
        }
    }

    private void CloseTextBox()
    {
        textBox.SetActive(false);
        Interacting = false;
        InteractionInProgress = false;
    }

    private void DoorAction()
    {
        InteractionInProgress = true;
        textBox.SetActive(true);
        Invoke("CloseTextBox", 4f);

        // TODO remove
        // SceneManager.LoadScene("3DOutside", LoadSceneMode.Single);
        // return;

        if (Anwser.handsWork)
        {
            textField.text = "You opened the Door!";
            SceneManager.LoadScene("3DOutside", LoadSceneMode.Single);
        }
        else
        {
            textField.text = "Unfortunately you lack the intellect to open this.  Learn basic motor skills!";
        }
    }

    private IEnumerator TaxiAction()
    {
        Debug.Log("canRead: " + Anwser.canRead + " canSocial: " + Anwser.canSocial);

        InteractionInProgress = true;
        /// TODO: remove this
        // SceneManager.LoadScene("3DNASA", LoadSceneMode.Single);
        //Anwser.canRead = true;
        //Anwser.canSocial = true;
        if (!Anwser.canRead)
        {
            yield return StartCoroutine(AudioManagers.main.PlaySFXCoroutine("taxi_conversation_learn_english"));
            textField.text = "Learn English!";
            textBox.SetActive(true);
            Invoke("CloseTextBox", 3f);
        }
        else if (!Anwser.canSocial)
        {
            //textField.text = "Hey man you need to work on your social skills.";
            yield return StartCoroutine(AudioManagers.main.PlaySFXCoroutine("learn_manners"));
            UbhLevelGenerator.levelSelected = 3;
            textField.text = "You learned social skills! Try out that newly trained tongue of yours.";
            SceneManager.LoadScene("UniBulletHell/Example/UBH_GameExample", LoadSceneMode.Additive);
            GameManager.DougLevelActive = false;
            GameManager.DougLevel = GameObject.FindGameObjectWithTag("CurrentLevel");
            Anwser.canSocial = true;
            InteractionInProgress = false;
            Interacting = false;
        }
        else if (Anwser.canSocial)
        {
            Debug.Log("About to go to nasa");
            yield return StartCoroutine(AudioManagers.main.PlaySFXCoroutine("save-world"));
            yield return StartCoroutine(PlayAllSkillSounds());
            SceneManager.LoadScene("3DNASA", LoadSceneMode.Single);
            //await AudioManagers.main.PlaySFXAsync("but-you-know");
        }
        Interacting = false;
    }

    private IEnumerator PlayAllSkillSounds()
    {
        Dictionary<string, bool> sounds = new Dictionary<string, bool>()
        {
            { "doorknob", Anwser.handsWork },
            { "add-numbers", Anwser.canMath },
            { "basic-speech", Anwser.canRead },
            { "trombone", Anwser.canMusic },
            { "see-colors", Anwser.canColor },
        };

        foreach (var sound in sounds)
        {
            if(sound.Value)
            {
                yield return StartCoroutine(AudioManagers.main.PlaySFXCoroutine(sound.Key));
            }
        }
    }

    private IEnumerator NasaGuyAction()
    {
        Debug.Log("NasaGuyAction");
        if (!Anwser.canPhysics && !InteractionInProgress)
        {
            InteractionInProgress = true;
            Debug.Log("about to play nasa sound");

            yield return StartCoroutine(AudioManagers.main.PlaySFXCoroutine("NASA_first_convo"));
            Debug.Log("finished playing nasa sound");

        }
        else
        {
            if (!finalSceneLoaded)
            {
                AudioManagers.main.StopSFX("full_trombone_song");
                Destroy(GameObject.FindWithTag("DontDestroy"));
                SceneManager.LoadScene("3DNASA Cutscene", LoadSceneMode.Single);
            }
            finalSceneLoaded = true;
        }
        Debug.Log("Setting interaction variables");
        InteractionInProgress = false;
        Interacting = false;
    }
}

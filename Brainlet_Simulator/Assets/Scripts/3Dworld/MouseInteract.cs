﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class MouseInteract : MonoBehaviour
{
    public GameObject Exclaim;
    public static bool Interacting = false; 

    public GameObject textBox;
    public Text Question;
    public Text A;
    public Text B;
    public Text C;

    public bool isACorrect = false;
    public bool isBCorrect = false;
    public bool isCCorrect = false;

    public string QuestionString = "";
    public string QuestionAString = "";
    public string QuestionBString = ""; 
    public string QuestionCString = "";

    public Anwser answer;
    
    void OnMouseOver()
    {
        
        Exclaim.SetActive(true);
        if (Interacting == false)
        {
            GameManager.currentInteraction = this.gameObject;
        }
    }

    void OnMouseExit()
    {
        
        Exclaim.SetActive(false);
    }

    void OnMouseUp()
    {
        Interacting = true;
    }
    
    // Character has reached object, show canvas so user can answer question
    public void QuestionTrigger()
    {
        textBox.SetActive(true);
        Question.text = QuestionString;
        A.text = QuestionAString;
        A.GetComponent<Anwser>().isCorrect = isACorrect;
        B.text = QuestionBString;
        B.GetComponent<Anwser>().isCorrect = isBCorrect;
        C.text = QuestionCString;
        C.GetComponent<Anwser>().isCorrect = isCCorrect;


    }

    private void OnTriggerStay(Collider other)
    {
        if(Interacting)
        {
            // only play audio once before question box open
            if(!textBox.activeSelf && GameManager.currentInteraction == GameObject.FindGameObjectWithTag("Music")) {
                AudioManagers.main.PlaySFX("frosty_snowman");
                AudioManagers.main.PlaySFX("full_trombone_song");
            }

            QuestionTrigger();

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public Vector3 offsetPos;
    public float moveSpeed = 5;
    public float turnSpeed = 10;
    public float smoothSpeed = 0.5f;

    Quaternion targetRotation;
    Vector3 targetPos;
    bool smoothRotating = false; 

	void LateUpdate () {
        MoveWithTarget();
        LookAtTarget();

        if (Input.GetMouseButtonDown(1))
        {
           // StartCoroutine("RotateAroundTarget", 90f);
        }
        /*if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            StartCoroutine("RotateAroundTarget", 45f);
        }*/
        if (Input.GetAxis("Mouse ScrollWheel") > 0f && offsetPos.z < 4) // forward
        {
            offsetPos.z++; 
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f && offsetPos.z > -4) // backwards
        {
            offsetPos.z--;
        }
    }

    void MoveWithTarget()
    {
        targetPos = target.position + offsetPos;
        transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);
    }

    void LookAtTarget()
    {
        targetRotation = Quaternion.LookRotation(target.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
    }
    IEnumerator RotateAroundTarget(float angle)
    {
        Vector3 vel = Vector3.zero;
        Vector3 targetOffsetPos = Quaternion.Euler(0, angle, 0) * offsetPos;
        float dist = Vector3.Distance(offsetPos, targetOffsetPos);
        smoothRotating = true;
        while(dist > 0.02f)
        {
            offsetPos = Vector3.SmoothDamp(offsetPos, targetOffsetPos, ref vel, smoothSpeed);
            dist = Vector3.Distance(offsetPos, targetPos);
            yield return null; 
        }
        smoothRotating = false;
        offsetPos = targetOffsetPos; 
    }
}
